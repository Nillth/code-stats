import { mount } from 'redom';

import { wait_for_load } from '../common/utils';
import TabComponent from '../common/tab.component';
import { AliasEditorComponent } from './preferences/alias-editor.component';
import { initExport } from './data_export_utils';

/**
 * Code to execute on preferences page. Adds functionality for the data export button.
 */

async function preferences_page() {
  await wait_for_load();
  initExport();

  const tab_container = document.getElementById('tab-container');
  mount(tab_container, new TabComponent([
    ['user-details', 'User details'],
    ['user-aliases', 'Language aliases'],
    ['change-password', 'Change password'],
    ['export-data', 'Export data'],
    ['delete-account', 'Delete account'],
  ], 'preferences-container'));

  const alias_el = document.getElementById('aliases-editor');
  const alias_textarea_el = document.getElementById('aliases');
  mount(alias_el, new AliasEditorComponent(alias_textarea_el));
}

export default preferences_page;
