// Import babel external helpers generated code
import '../common/babel-external-helpers';

import Router from './router';
import common_run from './common';

import {
  Chart,
  BarElement,
  LineElement,
  LineController,
  PointElement,
  BarController,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Tooltip
} from 'chart.js';
import '../common/chartjs-luxon-adapter';

Chart.register(
  BarElement,
  BarController,
  LineElement,
  LineController,
  PointElement,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Tooltip
);

common_run();

const router = new Router();
router.execute();
