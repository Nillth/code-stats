defmodule CodeStatsWeb.ProfileLive.Graphs.Utils do
  @doc """
  Get a hex colour matching the given language name.
  """
  @spec language_colour(String.t()) :: String.t()
  def language_colour(lang) do
    LanguageColours.get(lang, :colour_db)
  end
end
