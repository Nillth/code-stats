defmodule CodeStatsWeb.ProfileLive.DataProviders.CommonTypes do
  import CodeStats.Utils.TypedStruct

  @type provider_data() :: %{required(module()) => any()}

  defmodule XPStat do
    deftypedstruct(%{
      total_xp: {integer(), 0},
      recent_xp: {integer(), 0}
    })
  end
end
