defmodule CodeStatsWeb.ProfileLive.DataProviders.LastWeeksFlows do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      data: %{optional(Date.t()) => [CodeStats.User.Flow.t()]}
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(CodeStatsWeb.ProfileLive.DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    update_data(shared_data.cache)
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(_data, _user, _pulse, cache) do
    update_data(cache)
  end

  @spec update_data(Cache.t()) :: Data.t()
  defp update_data(cache) do
    now = Date.utc_today()
    then = Date.add(now, -CodeStatsWeb.ProfileUtils.last_days_amount())

    data =
      cache.flows
      |> Enum.filter(fn flow ->
        Date.compare(then, flow.start_time) in [:lt, :eq]
      end)
      |> Enum.group_by(&DateTime.to_date(&1.start_time))

    %Data{data: data}
  end
end
