defmodule CodeStatsWeb.ProfileLive.DataProviders.TopFlows do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.Profile.Queries.Flow
  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      longest: Flow.meta_flow() | nil,
      strongest: Flow.meta_flow() | nil,
      most_prolific: Flow.meta_flow() | nil
    })
  end

  @impl true
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) ::
          Data.t()
  def retrieve(shared_data, _user) do
    {:ok, meta_flows} = Flow.meta(shared_data.cache)

    %Data{
      longest: meta_flows.longest,
      strongest: meta_flows.strongest,
      most_prolific: meta_flows.most_prolific
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, _pulse, cache) do
    # Only the most recent flow can be updated live
    most_recent = List.last(cache.flows)

    if is_nil(most_recent) do
      data
    else
      most_recent_meta = Flow.format_meta_flow(most_recent)

      longest =
        Enum.max_by([data.longest, most_recent_meta], &maybe_val(&1, fn f -> f.duration end))

      strongest =
        Enum.max_by(
          [data.strongest, most_recent_meta],
          &maybe_val(&1, fn f -> Flow.flow_strength(f) end)
        )

      most_prolific =
        Enum.max_by([data.most_prolific, most_recent_meta], &maybe_val(&1, fn f -> f.xp end))

      %Data{
        longest: longest,
        strongest: strongest,
        most_prolific: most_prolific
      }
    end
  end

  @spec maybe_val(Flow.meta_flow() | nil, (Flow.meta_flow() -> integer() | float())) ::
          integer() | float()
  defp maybe_val(value, getter)
  defp maybe_val(nil, _getter), do: 0
  defp maybe_val(val, getter), do: getter.(val)
end
