defmodule CodeStatsWeb.ProfileLive.DataProviders.TopLanguages do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.XP
  alias CodeStatsWeb.ProfileUtils
  alias CodeStatsWeb.ProfileLive.DataProviders.SharedData
  alias CodeStatsWeb.ProfileLive.DataProviders.CommonTypes.XPStat

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  @type language_list :: [{String.t(), XPStat.t()}]

  defmodule Data do
    deftypedstruct(%{
      top_languages: CodeStatsWeb.ProfileLive.DataProviders.TopLanguages.language_list(),
      other_languages: CodeStatsWeb.ProfileLive.DataProviders.TopLanguages.language_list()
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(SharedData.data_key())
  def required_data(), do: MapSet.new([:cache, :recent_pulses])

  @impl true
  @spec retrieve(SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    cached_languages =
      ProfileUtils.process_language_xps(shared_data.cache.languages)
      |> Enum.into(%{}, fn {k, v} -> {k.name, %XPStat{total_xp: v}} end)

    data =
      Enum.reduce(shared_data.recent_pulses, cached_languages, fn %Pulse{} = p, acc ->
        Enum.reduce(p.xps, acc, fn %XP{} = x, inner_acc ->
          case Map.get(inner_acc, x.language.name) do
            nil ->
              inner_acc

            %XPStat{} = stat ->
              Map.put(inner_acc, x.language.name, %XPStat{
                total_xp: stat.total_xp,
                recent_xp: stat.recent_xp + x.amount
              })
          end
        end)
      end)

    {top, other} = data |> sort_data() |> Enum.split(top_count())

    %Data{
      top_languages: top,
      other_languages: other
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(%Data{top_languages: top, other_languages: other}, _user, pulse, _cache) do
    data = top ++ other
    old_data = Map.new(data)

    new_data =
      Enum.reduce(pulse.xps, old_data, fn %XP{} = xp, acc ->
        existing = Map.get(old_data, xp.language.name, %XPStat{})

        new = %XPStat{
          existing
          | total_xp: existing.total_xp + xp.amount,
            recent_xp: existing.recent_xp + xp.amount
        }

        Map.put(acc, xp.language.name, new)
      end)
      |> Map.to_list()
      |> sort_data()

    {top, other} = Enum.split(new_data, top_count())

    %Data{
      top_languages: top,
      other_languages: other
    }
  end

  @spec sort_data(language_list()) :: language_list()
  defp sort_data(data) do
    data
    |> Enum.sort(fn {_, %XPStat{} = a}, {_, %XPStat{} = b} ->
      a.total_xp <= b.total_xp
    end)
    |> Enum.reverse()
  end

  @spec top_count() :: pos_integer()
  defp top_count(), do: Application.get_env(:code_stats, :profile_top_languages_count, 10)
end
