defmodule CodeStatsWeb.ProfileLive.DataProviders.TopMachines do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.XP
  alias CodeStatsWeb.ProfileUtils
  alias CodeStatsWeb.ProfileLive.DataProviders.SharedData
  alias CodeStatsWeb.ProfileLive.DataProviders.CommonTypes.XPStat

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  @type machine_list :: [{pos_integer(), {String.t(), XPStat.t()}}]

  defmodule Data do
    deftypedstruct(%{
      top_machines: CodeStatsWeb.ProfileLive.DataProviders.TopMachines.machine_list()
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(SharedData.data_key())
  def required_data(), do: MapSet.new([:cache, :recent_pulses])

  @impl true
  @spec retrieve(SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, user) do
    cached_machines =
      ProfileUtils.process_machine_xps(shared_data.cache.machines, user)
      |> Enum.into(%{}, fn {k, v} -> {k.id, {k.name, %XPStat{total_xp: v}}} end)

    data =
      Enum.reduce(shared_data.recent_pulses, cached_machines, fn %Pulse{} = p, acc ->
        Enum.reduce(p.xps, acc, fn %XP{} = x, inner_acc ->
          case Map.get(inner_acc, p.machine.id) do
            nil ->
              inner_acc

            {name, %XPStat{} = stat} ->
              Map.put(
                inner_acc,
                p.machine.id,
                {name,
                 %XPStat{
                   total_xp: stat.total_xp,
                   recent_xp: stat.recent_xp + x.amount
                 }}
              )
          end
        end)
      end)

    %Data{
      top_machines: sort_data(data)
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(%Data{top_machines: data}, _user, pulse, _cache) do
    old_data = Map.new(data)

    new_data =
      Enum.reduce(pulse.xps, old_data, fn %XP{} = xp, acc ->
        existing = Map.get(old_data, pulse.machine.id, {nil, %XPStat{}}) |> elem(1)

        new = %XPStat{
          existing
          | total_xp: existing.total_xp + xp.amount,
            recent_xp: existing.recent_xp + xp.amount
        }

        Map.put(acc, pulse.machine.id, {pulse.machine.name, new})
      end)
      |> Map.to_list()
      |> sort_data()

    %Data{
      top_machines: new_data
    }
  end

  @spec sort_data(machine_list()) :: machine_list()
  defp sort_data(data) do
    data
    |> Enum.sort(fn {_, {_, %XPStat{} = a}}, {_, {_, %XPStat{} = b}} ->
      a.total_xp <= b.total_xp
    end)
    |> Enum.reverse()
  end
end
