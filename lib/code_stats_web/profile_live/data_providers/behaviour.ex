defmodule CodeStatsWeb.ProfileLive.DataProviders.Behaviour do
  alias CodeStatsWeb.ProfileLive.DataProviders.SharedData

  alias CodeStats.User
  alias CodeStats.User.Pulse

  @doc """
  Return set of keys of data required by this provider.
  """
  @callback required_data() :: MapSet.t(SharedData.data_key())

  @doc """
  Retrieve the initial data to show when the page is loaded.
  """
  @callback retrieve(SharedData.t(), User.t()) :: any()

  @doc """
  Update the given data with the given new Pulse.
  """
  @callback update(any(), User.t(), Pulse.t(), User.Cache.t()) :: any()
end
