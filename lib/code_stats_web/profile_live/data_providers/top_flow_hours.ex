defmodule CodeStatsWeb.ProfileLive.DataProviders.TopFlowHours do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.User.Flow
  alias CodeStatsWeb.ProfileLive.DataProviders

  @behaviour DataProviders.Behaviour

  @typep flow_hour_data :: %{
           optional(pos_integer()) => pos_integer()
         }

  defmodule Data do
    deftypedstruct(%{
      data: [{String.t(), integer()}]
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    update_data(shared_data.cache.flows)
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(_data, _user, _pulse, cache) do
    update_data(cache.flows)
  end

  @spec update_data([Flows.t()]) :: Data.t()
  defp update_data(flows) do
    flow_hours =
      Enum.reduce(flows, %{}, fn %Flow{} = flow, acc ->
        hours =
          calculate_flow_hours_natsu(
            flow.start_time_local.hour,
            flow.start_time_local.minute,
            flow.duration
          )

        Map.merge(acc, hours, fn _, m1, m2 -> m1 + m2 end)
      end)

    data =
      for {hour, minutes} <- flow_hours do
        padded_hour = hour |> to_string() |> String.pad_leading(2, "0")
        {padded_hour, minutes}
      end

    %Data{
      data: data
    }
  end

  @spec calculate_flow_hours_natsu(
          Calendar.ISO.hour(),
          Calendar.ISO.minute(),
          pos_integer(),
          flow_hour_data()
        ) :: flow_hour_data()
  defp calculate_flow_hours_natsu(hour, minute, duration, acc \\ %{})

  defp calculate_flow_hours_natsu(_, 0, 0, acc) do
    acc
  end

  defp calculate_flow_hours_natsu(hour, minute, duration, acc) do
    minutes = min(duration, 60 - minute)
    new_acc = Map.update(acc, hour, minutes, &(&1 + minutes))
    calculate_flow_hours_natsu(rem(hour + 1, 24), 0, duration - minutes, new_acc)
  end
end
