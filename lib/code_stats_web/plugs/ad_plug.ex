defmodule CodeStatsWeb.AdPlug do
  @moduledoc """
  Plug to control if ads are shown for the current user.
  """

  alias CodeStats.Utils
  alias CodeStats.User

  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  @spec call(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def call(conn, _opts) do
    current_user = CodeStatsWeb.AuthUtils.get_current_user(conn)
    Plug.Conn.assign(conn, :show_ads, show_ad?(conn.query_params, current_user, nil))
  end

  @doc """
  Should an ad be shown for the current user or the user being viewed?
  """
  @spec show_ad?(Plug.Conn.query_params(), User.t() | nil, User.t() | nil) :: boolean()
  def show_ad?(query_params, _current_user, _viewing_user) do
    ads_enabled = Utils.get_conf(:ads_enabled)
    query_ads = Map.get(query_params, "show_ads") == "true"

    ads_enabled and query_ads
  end
end
